#include <iostream>

using namespace std;

int main() {
    cout << "How many numbers?" << endl;
    int amount;
    cin >> amount;
    int Num[amount] = {};
    int Placeholder[2] = {};
    for (int k = 0; k != amount; k++) {
        cin >> Num[k];
    }
    bool has_not_been_edited = false;
    while(has_not_been_edited == false) {
        has_not_been_edited = true;
        for (int a = 0; a != amount; a++) {
            int b = a + 1;
            if (Num[a] > Num[b]) {
                Placeholder[0] = Num[a];
                Placeholder[1] = Num[b];
                Num[a] = Placeholder[1];
                Num[b] = Placeholder[0];
                has_not_been_edited = false;
                //Debug
                for (int asdf = 0; asdf != amount; asdf++) {
                    cout << Num[asdf] << endl;
                }
            }
        }
    }
    cout << "Output:" << endl;
    for (int ending = 0; ending != amount; ending++) {
        cout << Num[ending] << endl;
    }
    return 0;
}
